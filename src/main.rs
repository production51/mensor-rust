#[macro_use] extern crate rocket;
use rocket::{
	serde::json::Json,
	http::Status,
};

mod domain;
mod storage;

use domain::measure::Measure;
use storage::in_memory::InMemoryStorage;

// List available metric names
#[get("/metric")]
fn get_metrics(storage: &rocket::State<InMemoryStorage>) -> Json<Vec<String>> {
	Json(storage.get_metrics())
}

// Fetch measures belonging to the given metric name
#[get("/metric/<name>")]
fn get_measures(name: String, storage: &rocket::State<InMemoryStorage>) -> Json<Vec<Measure>> {
	Json(storage.get_measures(name))
}

// Post a measure for a metric
#[post("/metric/<name>", format = "json", data = "<measure>")]
fn add_measure(name: String, measure: Json<Measure>, storage: &rocket::State<InMemoryStorage>) -> Status {
    storage.add_measure(name, measure.into_inner());
	Status::Created
}

#[launch]
fn rocket() -> rocket::Rocket<rocket::Build> {
	let storage = InMemoryStorage::new();
    rocket::build()
		.mount("/", routes![
			get_metrics, 
			get_measures,
			add_measure,
		])
		.manage(storage)
}
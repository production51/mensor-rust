
use rocket::{
	serde::{Serialize, Deserialize},
};

#[derive(Serialize, Deserialize, Clone, Copy)]
#[serde(crate = "rocket::serde")]
pub struct Measure {
	pub value: f32,
	pub timestamp: i64,
}
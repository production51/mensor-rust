use std::sync::Mutex;
 use std::collections::HashMap;
use crate::domain::measure::Measure;

pub struct InMemoryStorage {
	measures: Mutex<HashMap<String, Vec<Measure>>>
}

impl InMemoryStorage {
	pub fn new() -> InMemoryStorage {
		InMemoryStorage{
			measures: Mutex::new(HashMap::new()),
		}
	}

	pub fn get_metrics(&self) -> Vec<String> {
		let lock = self.measures.lock().expect("lock shared data");
		lock.keys().map(|key| key.clone()).collect()
	}

	pub fn get_measures(&self, metric_name: String) -> Vec<Measure> {
		let lock = self.measures.lock().expect("lock shared data");
		match lock.get(&metric_name) {
			None => Vec::new(),
			Some(measures) => measures.clone(),
		}
	}

	pub fn add_measure(&self, metric_name: String, measure: Measure) {
		let mut lock = self.measures.lock().expect("lock shared data");
		match lock.get_mut(&metric_name) {
			None => {
				lock.insert(metric_name, vec![measure]);
			},
			Some(measures) => measures.push(measure),
		};
	}
}
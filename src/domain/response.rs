#[derive(Responder)]
#[response(status = 200, content_type = "json")]
struct AcceptedRawJson(&'static str);